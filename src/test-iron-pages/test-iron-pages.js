import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';


import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';

/**
 * @customElement
 * @polymer
 */
class TestIronPages extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h1 class="jumbotron"> Soy el test iron pages</h1>

<select placeholder="Selecionar componente" value="{{componentName::change}}">
  <option value=" ">Seleccionar componente</option>
  <option value="visor-usuario">Visor usuario</option>
  <option value="login-usuario">Login usuario</option>
</select>

<iron-pages selected="[[componentName]]" attr-for-selected="component-name">
  <div component-name="visor-usuario"><visor-usuario id="4"></visor-usuario></div>
  <div component-name="login-usuario"><login-usuario></login-usuario></div>
</iron-pages>

    `;
  }//get template
  static get properties() {
    return {
      componentName :{
        type :String
      }
    };
  }//properties




}//class TestIronPages

window.customElements.define('test-iron-pages', TestIronPages);
