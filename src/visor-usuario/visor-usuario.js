import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;

          //all: initial; // para que no herede estilos de la pagina que lo contiene
        }
        .redbg{
          background-color: red;
        }
        .greenbg{
          background-color: green;
        }
        .bluebg{
          background-color: blue;
        }
        .greybg{
          background-color: grey;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2 >Soy  [[first_name]] [[last_name]]</h2>
      <h3>y mi email es   [[email]]</h3>

      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="boton">
          <button on-click="showAccounts" class="btn btn-dark">Ver cuentas </button>
        </div>
        <div component-name="tabla-cuentas">
          <visor-cuentas id="visorCuenta"></visor-cuetas>
        </div>
      </iron>
      <!--
      <div class="row greybg">
          <div class="col-2 col-sm-6 redbg">col 1</div>
          <div class="col-3 col-sm-1 greenbg">col 2</div>
          <div class="col-4 offset-2 bluebg">col 3</div>

      </div>
      <button class="btn btn-primary btn-lg">Login </button>
      <button class="btn btn-secondary btn-sm">Logout </button>
      <button class="btn btn-success btn-md">Login </button>
      <button class="btn btn-warning btn-lg">Logout </button>
      <button class="btn btn-info btn-xl">Login </button>
      <button class="btn btn-danger">Logout </button>
      <button class="btn btn-light">Login </button>
      <button class="btn btn-dark">Logout </button>

-->
      <iron-ajax

        id="getUser"
        url="http://localhost:3000/apitechu/v2/users/{{userid}}"
        handle-as="json"
        on-response="showData">
      </iron-ajax>
    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },
      last_name: {
        type: String
      },
      email: {
        type: String
      },
      userid:{
        type: Number,
        observer:"_useridChanged"
      },
      componentName :{
          type :String,
          value:"boton"
        }
    };
  }//properties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name = data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;


  }//showData(data)

  _useridChanged(newValue, oldValue) {
    this.$.getUser.generateRequest();
  }

  showAccounts(){
    this.$.visorCuenta.userid = this.userid;
    this.componentName = "tabla-cuentas";
  }


}//class VisorUsuario

window.customElements.define('visor-usuario', VisorUsuario);
