import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;


        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2 Login Usuario</h2>
      <input type="email" placeholder="email" value="{{email::input}}"></input>
      <input type="password" placeholder="password" value="{{password::input}}"></input>
      <button on-click="onLogin" >Login</button>
      <span hidden$={{!isLogged}}>Bienvenido/a de nuevo</span>

      <iron-ajax
        id="doLogin"
        url="http://localhost:3000/apitechu/v2/login"
        handle-as="json"
        method="POST"
        content-type="application/json"
        on-response="manageAJAXResponse"
        on-error="showError"
        ></iron-ajax>
    `;
  }
  static get properties() {
    return {

      email: {
        type: String,
        value :"uno@uno.org"
      },
      password:{
        type: String,
        value:"uno"
      },
      isLogged:{
        type: Boolean,
        value: false
      }
    };
  }//properties

onLogin(){
  console.log("El usuario a pulsado el botón ");
  console.log("voy a enviar la petición");
  var loginData= {
    "email" : this.email,
    "password" : this.password
    }
  console.log(loginData);
  this.$.doLogin.body = JSON.stringify(loginData); // $ para hacer referencia a un elemenot del DOM, body es donde mandamos los datos, pero es un string que se puede covertir en JSON'
  this.$.doLogin.generateRequest();// esta función es la que hace el auto por defecto
  console.log("peticion enviada");

}//login

  showError(error){
    console.log("ha devuelto un 400 en el status de la petición");
    console.log(error);
  }//showError

  //si va bien la llamada iron-ajax, se ejecuta esta peticion
  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.isLogged=true;

    this.sendEvent(data.detail.response);

  }//manageAJAXResponse(data)


  sendEvent(e){
    console.log("el usuario ha pulsado el botón");
    console.log(e);

    this.dispatchEvent(
      new CustomEvent(
        "loginsucess",
        {
          "detail": {
            "userid":e.id
          }//detail
        }
      )//CustomEvent
    )//dispatchEvent

  }//sendEvent


}//class VisorUsuario

window.customElements.define('login-usuario', LoginUsuario);
