import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

/**
 * @customElement
 * @polymer
 */
class VisorAccount extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;

        }
        .greybg{
          background-color: grey;
        }

      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h2 >Las cuentas del userId {{userid}} </h2>



            <div class="row offset-1">
            <div class=" col-3  greybg">IBAN:</div>

            <div class="col-2 greybg">Balance: </div>
            </div>
          <dom-repeat items="{{accounts}}">
            <template>
              <div class="row offset-1">
              <div class="col-3"><span>{{item.IBAN}}</span></div>

              <div class="col-2"><span>{{item.balance}}</span></div>
              </div>
            </template>
          </dom-repeat>


      <iron-ajax

        id="getAccounts"
        url="http://localhost:3000/apitechu/v1/accounts/{{userid}}"
        handle-as="json"
        on-response="showData">
      </iron-ajax>
    `;
  }

    static get properties() {
      return {
        accounts:{type: Array},
        userid: {type: String ,
       observer: "_useridChanged" }


      };
    }

    _useridChanged(newValue, oldValue){
      console.log("id en cuentas value has changed");
      console.log("Old value was "+oldValue);
      console.log("New value is "+newValue);

      if (newValue)
        {
          this.$.getAccounts.generateRequest();
        }

    }//_useridChanged

  showData(data){
    console.log("showData cuentas");

    console.log(data.detail.response);
    this.accounts = data.detail.response;


  }//showData(data)
}//class VisorAccount

window.customElements.define('visor-cuentas', VisorAccount);
