import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';

/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

      <h1> Soy el Panel Usuario, yo soy tu padre</h1>


      <iron-pages selected="[[componentName]]" attr-for-selected="component-name">
        <div component-name="login-usuario">
            <login-usuario id="loginUsuario" on-loginsucess="processLoginSuccess"></login-usuario>
        </div>
        <div component-name="visor-usuario">
            <visor-usuario id="visorUsuario"></visor-usuario>
        </div>
      </iron-pages>

    `;
  }//get template
  static get properties() {
    return {
      componentName :{
        type :String,
        value:"login-usuario"
      }
    };
  }//properties

  processLoginSuccess(e){
    console.log("Capturado el evento del emisor");
    console.log(e.detail);

    this.$.visorUsuario.userid = e.detail.userid;
    this.componentName = "visor-usuario";
  }//processLoginSuccess

}//class PanelUsuario

window.customElements.define('panel-usuario', PanelUsuario);
